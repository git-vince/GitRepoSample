package net.therapbd.sample.gitrepo.view.activities;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.therapbd.sample.gitrepo.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class UITest {
    private ViewInteraction recyclerview;

    @Before
    public void init() {
        ActivityScenario activityScenario = ActivityScenario.launch(MainActivity.class);
        recyclerview = onView(withId(R.id.recyclerView));
        ViewInteraction swipeToRefresh = onView(withId(R.id.swipeToRefresh));
    }

    @Test
    public void uiTest() {
        loadList();
        checkList();
        clickOnRepository("codemod");
        //IntentSubject.assertThat(getIntent)

        writeNote("Lets not save this note");
        exitWithoutSaving();

        loadList();
        clickOnRepository("facebook-android-sdk");
        writeNote("Let user save this note with the save button");
        saveNoteWithButton();

        clickOnRepository("hhvm");
        writeNote("Press Back and select Save and exit");
        saveAndExit();
    }


    private void loadList() {
        recyclerview.perform(swipeDown());
    }

    private void checkList() {
        recyclerview.check(matches(hasDescendant(withText("codemod"))));
    }

    private void clickOnRepository(String repository) {
        onView(withText(repository)).perform(click());
    }

    private void writeNote(String text) {
        ViewInteraction textField = onView(withId(R.id.note));
        textField.perform(click());
        textField.perform(typeText(text));
        textField.perform(closeSoftKeyboard());
    }

    private void exitWithoutSaving() {
        backPress(onView(withId(R.id.note)));
        onView(withText("Unsaved Changes!")).check(matches(isDisplayed()));
        onView(withText("Discard")).perform(click());
    }

    private void saveAndExit() {
        backPress(onView(withId(R.id.note)));
        onView(withText("Unsaved Changes!")).check(matches(isDisplayed()));
        onView(withText("Save and Exit")).perform(click());
    }


    private void backPress(ViewInteraction viewInteraction) {
        viewInteraction.perform(pressBack());
    }

    private void saveNoteWithButton() {
        ViewInteraction save = onView(withId(R.id.save));
        save.perform(click());
        backPress(save);
    }

    @After
    public void halt() {
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}