package net.therapbd.sample.gitrepo.viewmodel;

import net.therapbd.sample.gitrepo.model.Repository;
import net.therapbd.sample.gitrepo.network.api.Organisation;
import net.therapbd.sample.gitrepo.network.client.NetworkClient;
import net.therapbd.sample.gitrepo.view.adapter.RepositoryListAdapter;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class MainViewModelTest {

    MainViewModel mainViewModel;

    @Before
    public void setup() {
        mainViewModel = new MainViewModel();

    }

    @Test
    public void set_get_repositoryList() {
        List<Repository> repositories = new ArrayList<>();

        mainViewModel.setAdapter(new RepositoryListAdapter(repositories));

        assertArrayEquals(repositories.toArray(), mainViewModel.getRepositoryList().toArray());

        repositories.add(new Repository());

        assertNotEquals(repositories.size(), mainViewModel.getRepositoryList().size());
    }

    @Test
    public void set_get_Adapter() {
        assertNotNull(mainViewModel.getAdapter());

        mainViewModel.setAdapter(null);
        assertNull(mainViewModel.getAdapter());
    }

    @Test
    public void testRepositoryApiResponse() {

        try {
            Response<ResponseBody> response = NetworkClient.getApiService().requestRepositoryList(Organisation.facebook.name()).execute();

            assertTrue(response.isSuccessful());

            assertEquals(response.code(), 200);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}