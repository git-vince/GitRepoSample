package net.therapbd.sample.gitrepo.viewmodel;

import net.therapbd.sample.gitrepo.util.AppUtils;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class NoteViewModelTest {

    private NoteViewModel noteViewModel;

    @Before
    public void setup() {
        noteViewModel = new NoteViewModel(5);
    }

    @Test
    public void set_get_Note() {
        noteViewModel.setNote("hello");
        assertEquals("hello", noteViewModel.getNote());

        noteViewModel.setNote(null);
        assertNull(noteViewModel.getNote());

        assertFalse(noteViewModel.isNoteDataChanged.get());
    }

    @Test
    public void set_get_History() {
        noteViewModel.setHistory(AppUtils.noteLastSavedTimeToString(0));
        assertEquals("n/a", noteViewModel.getHistory());

        noteViewModel.setHistory(AppUtils.noteLastSavedTimeToString(-99));
        assertEquals("n/a", noteViewModel.getHistory());


        noteViewModel.setHistory(AppUtils.noteLastSavedTimeToString(1));
        assertEquals("Thu, 01 Jan 1970 06:00:00 BDT", noteViewModel.getHistory());

        noteViewModel.setHistory(AppUtils.noteLastSavedTimeToString(1507602200000L));
        assertEquals("Tue, 10 Oct 2017 08:23:20 BDT", noteViewModel.getHistory());
    }
}