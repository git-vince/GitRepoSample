package net.therapbd.sample.gitrepo.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AppUtilsTest {

    @Test
    public void noteLastSavedTimeToString() {
        assertEquals("n/a", AppUtils.noteLastSavedTimeToString(-99));
        assertEquals("n/a", AppUtils.noteLastSavedTimeToString(0));
        assertEquals("Thu, 01 Jan 1970 06:00:00 BDT", AppUtils.noteLastSavedTimeToString(1));
        assertEquals("Tue, 10 Oct 2017 08:23:20 BDT", AppUtils.noteLastSavedTimeToString(1507602200000L));
    }
}