package net.therapbd.sample.gitrepo.databinding;

import java.util.List;

public interface BindableListAdapter<T> {
    void setData(List<T> items);
}
