package net.therapbd.sample.gitrepo.network.webservice;


import net.therapbd.sample.gitrepo.model.Repository;

import java.util.List;

public interface RepositoryCallback {
    void success(List<Repository> repositories);

    void failed(String errorMessage);
}
