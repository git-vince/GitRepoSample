package net.therapbd.sample.gitrepo.viewmodel;

import androidx.databinding.Bindable;

import net.therapbd.sample.gitrepo.BR;
import net.therapbd.sample.gitrepo.database.DatabaseTransactions;
import net.therapbd.sample.gitrepo.model.Repository;
import net.therapbd.sample.gitrepo.network.webservice.RepositoryCallback;
import net.therapbd.sample.gitrepo.network.webservice.RepositoryService;
import net.therapbd.sample.gitrepo.util.AppUtils;
import net.therapbd.sample.gitrepo.view.adapter.RepositoryListAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class MainViewModel extends BaseViewModel {

    private List<Repository> repositoryList;

    private RepositoryListAdapter adapter;

    @Inject
    public MainViewModel() {
        repositoryList = new ArrayList<>();
        adapter = new RepositoryListAdapter(repositoryList);
    }

    @Bindable
    public List<Repository> getRepositoryList() {
        return repositoryList;
    }

    @Bindable
    public RepositoryListAdapter getAdapter() {
        return adapter;
    }

    @Bindable
    public void setAdapter(RepositoryListAdapter adapter) {
        this.adapter = adapter;
        notifyPropertyChanged(BR.adapter);
    }

    public void populateRepositoryList() {
        loadDataFromDB();
        fetch();
    }

    private void fetch() {
        isLoading.set(true);
        RepositoryService.getRepositoryService().fetchRepositoryList(new RepositoryCallback() {
            @Override
            public void success(List<Repository> repositories) {
                if (adapter == null) {
                    adapter = new RepositoryListAdapter(repositories); //will be solved when dagger gets introduced
                    setAdapter(adapter);
                } else {
                    setRepositoryList(repositories);
                }
                isLoading.set(false);
                isNoDataFound.set(false);
            }

            @Override
            public void failed(String errorMessage) {
                if (repositoryList.isEmpty())
                    isNoDataFound.set(true);
                isLoading.set(false);
            }
        });
    }

    private void loadDataFromDB() {
        setRepositoryList(AppUtils.getRepositoryListFromRealmResults(DatabaseTransactions.getDatabaseTransactions().retrieveRepositories()));
    }

    @Bindable
    public void setRepositoryList(List<Repository> repositoryList) {
        this.repositoryList = repositoryList;
        notifyPropertyChanged(BR.repositoryList);
    }

}
