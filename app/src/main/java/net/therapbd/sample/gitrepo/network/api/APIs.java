package net.therapbd.sample.gitrepo.network.api;

import android.util.Log;

import static net.therapbd.sample.gitrepo.network.api.PathConstant.ORG_NAME_PATH;

public class APIs {
    public static final String BASE_URL = "https://api.github.com/";
    private static final String orgRepositories = "orgs/" + ORG_NAME_PATH + "/repos";

    public static String getOrgName(Organisation organisation) {
        urlResolver(orgRepositories);
        return organisation.name();
    }

    private static void urlResolver(String url) {
        Log.d("okhttp","--> "+BASE_URL+url);
    }

}

