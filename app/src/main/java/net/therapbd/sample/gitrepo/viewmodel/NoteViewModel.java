package net.therapbd.sample.gitrepo.viewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import net.therapbd.sample.gitrepo.BR;
import net.therapbd.sample.gitrepo.database.DatabaseTransactions;
import net.therapbd.sample.gitrepo.model.RepositoryNote;
import net.therapbd.sample.gitrepo.util.AppUtils;

public class NoteViewModel extends BaseViewModel {
    public ObservableBoolean isNoteDataChanged = new ObservableBoolean(false);
    private int repositoryId;
    private String note;
    private String history;

    public NoteViewModel(int repositoryId) {
        this.repositoryId = repositoryId;
    }

    @Bindable
    public String getNote() {
        isNoteDataChanged.set(false);
        return note;
    }

    @Bindable
    public void setNote(String note) {
        this.note = note;
        isNoteDataChanged.set(true);
    }

    @Bindable
    public String getHistory() {
        return history;
    }

    @Bindable
    public void setHistory(String history) {
        this.history = history;
        notifyPropertyChanged(BR.note);
    }

    public void saveNote() {
        DatabaseTransactions.getDatabaseTransactions().createOrUpdateRepositoryNote(new RepositoryNote(repositoryId, getNote(), System.currentTimeMillis()));
        isNoteDataChanged.set(false);
    }

    public void loadNoteFromDB() {
        RepositoryNote repositoryNote = DatabaseTransactions.getDatabaseTransactions().retrieveRepositoryNote(repositoryId);
        if (repositoryNote != null) {
            setNoteDataToView(repositoryNote);
        }
    }

    private void setNoteDataToView(RepositoryNote repositoryNote) {
        setNote(repositoryNote.getNote());
        setHistory(AppUtils.noteLastSavedTimeToString(repositoryNote.getLastSavedTime()));
    }

}
