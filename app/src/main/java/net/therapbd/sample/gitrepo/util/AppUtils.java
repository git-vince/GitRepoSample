package net.therapbd.sample.gitrepo.util;

import android.content.Context;
import android.view.View;

import androidx.core.content.ContextCompat;

import net.therapbd.sample.gitrepo.database.dbmodel.RepositoryDB;
import net.therapbd.sample.gitrepo.model.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.RealmResults;

public class AppUtils {
    public static int getColor(Context context, int colorResourceId) {
        return ContextCompat.getColor(context, colorResourceId);
    }

    public static boolean isVisible(View view) {
        return view.getVisibility() == View.VISIBLE;
    }

    public static String noteLastSavedTimeToString(long lastEditHistory) {
        if (lastEditHistory <= 0)
            return "n/a";
        else
            return millisToDateString(lastEditHistory);
    }

    private static String millisToDateString(long millis) {
        Date d = new Date();
        d.setTime(millis);

        DateFormat dateFormat = new SimpleDateFormat(AppConstant.DATE_AND_TIME_PATTERN, Locale.getDefault());
        return dateFormat.format(d);
    }

    public static List<Repository> getRepositoryListFromRealmResults(RealmResults<RepositoryDB> realmResults) {
        List<Repository> repositoryList = new ArrayList<>();

        if (realmResults != null)
            for (RepositoryDB repositoryDB : realmResults)
                repositoryList.add(new Repository().clone(repositoryDB));

        return repositoryList;
    }
}
