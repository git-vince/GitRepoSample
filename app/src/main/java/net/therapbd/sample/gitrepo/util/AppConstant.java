package net.therapbd.sample.gitrepo.util;

public class AppConstant {
    public static final String DATE_AND_TIME_PATTERN = "E, dd MMM yyyy HH:mm:ss z";
    public static final String INTENT_ID = "intent_id";
    public static final String INTENT_NAME = "intent_name";
    public static final String INTENT_SUB_TITLE = "intent_sub_title";
    public static final int REPOSITORY_ID_NOT_FOUND = -1;
}
