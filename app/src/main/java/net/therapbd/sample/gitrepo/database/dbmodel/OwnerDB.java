package net.therapbd.sample.gitrepo.database.dbmodel;

import io.realm.RealmObject;

public class OwnerDB extends RealmObject {

    private String login;
    private String avatar_url;  //Realm can't map underscored json property to CamelCase object

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatar_url;
    }

    public void setAvatarUrl(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
