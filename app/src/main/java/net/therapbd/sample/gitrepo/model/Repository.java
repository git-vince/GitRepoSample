package net.therapbd.sample.gitrepo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.therapbd.sample.gitrepo.database.dbmodel.RepositoryDB;

import io.realm.annotations.PrimaryKey;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Repository {

    @PrimaryKey
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("owner")
    private Owner owner;
    @JsonProperty("description")
    private String description;
    @JsonProperty("fork")
    private Boolean fork;

    public Repository clone(RepositoryDB repositoryDB) {
        setId(repositoryDB.getId());
        setName(repositoryDB.getName());

        Owner owner = new Owner();
        owner.clone(repositoryDB.getOwner());
        setOwner(owner);

        setDescription(repositoryDB.getDescription());
        setFork(repositoryDB.getFork());
        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFork() {
        return fork;
    }

    public void setFork(Boolean fork) {
        this.fork = fork;
    }
}
