package net.therapbd.sample.gitrepo.network.webservice;


import net.therapbd.sample.gitrepo.database.DatabaseTransactions;
import net.therapbd.sample.gitrepo.network.api.APIs;
import net.therapbd.sample.gitrepo.network.api.Organisation;
import net.therapbd.sample.gitrepo.network.client.NetworkClient;
import net.therapbd.sample.gitrepo.network.client.ResponseCode;
import net.therapbd.sample.gitrepo.util.AppUtils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepositoryService {
    private static RepositoryService repositoryService;

    private RepositoryService() {
    }

    public static synchronized RepositoryService getRepositoryService() {
        if (repositoryService == null)
            repositoryService = new RepositoryService();
        return repositoryService;
    }

    public void fetchRepositoryList(RepositoryCallback repositoryCallback) {

        Call<ResponseBody> call = NetworkClient.getApiService().requestRepositoryList(APIs.getOrgName(Organisation.facebook));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == ResponseCode.SUCCESSFUL && response.body() != null) {
                    try {
                        DatabaseTransactions.getDatabaseTransactions().insertRepositories(response.body().string());
                        repositoryCallback.success(AppUtils.getRepositoryListFromRealmResults(DatabaseTransactions.getDatabaseTransactions().retrieveRepositories()));
                    } catch (IOException e) {
                        e.printStackTrace();
                        repositoryCallback.failed(e.getLocalizedMessage());
                    }
                } else {
                    repositoryCallback.failed(String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                repositoryCallback.failed(t.getLocalizedMessage());
            }
        });


    }

}
