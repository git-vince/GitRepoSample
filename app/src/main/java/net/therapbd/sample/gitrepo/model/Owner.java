package net.therapbd.sample.gitrepo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.therapbd.sample.gitrepo.database.dbmodel.OwnerDB;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Owner {

    @JsonProperty("login")
    private String login;
    @JsonProperty("avatar_url")
    private String avatar_url;  //Realm can't map underscored json property to CamelCase object


    public void clone(OwnerDB ownerDB) {
        setLogin(ownerDB.getLogin());
        setAvatarUrl(ownerDB.getAvatarUrl());
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatar_url;
    }

    public void setAvatarUrl(String avatar_url) {
        this.avatar_url = avatar_url;
    }


}
