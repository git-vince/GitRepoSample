package net.therapbd.sample.gitrepo.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import net.therapbd.sample.gitrepo.BR;
import net.therapbd.sample.gitrepo.R;
import net.therapbd.sample.gitrepo.databinding.BindableListAdapter;
import net.therapbd.sample.gitrepo.model.Repository;
import net.therapbd.sample.gitrepo.util.AppConstant;
import net.therapbd.sample.gitrepo.view.activities.NoteActivity;

import java.util.List;

import javax.inject.Inject;

public class RepositoryListAdapter extends RecyclerView.Adapter<RepositoryListAdapter.ViewHolder> implements BindableListAdapter<Repository> {

    private List<Repository> repositories;

    @Inject
    public RepositoryListAdapter(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @NonNull
    @Override
    public RepositoryListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ViewDataBinding itemView = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_git_repo, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RepositoryListAdapter.ViewHolder holder, int position) {
        holder.bind(repositories.get(position));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    @Override
    public void setData(List<Repository> items) {
        setRepositories(items);
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return repositories == null || repositories.isEmpty();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding binding;

        ViewHolder(@NonNull final ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(v -> startNoteActivity(binding.getRoot().getContext(), repositories.get(getLayoutPosition())));
        }

        private void startNoteActivity(Context context, final Repository repositoryDB) {
            Intent intent = new Intent(context, NoteActivity.class);
            intent.putExtra(AppConstant.INTENT_ID, repositoryDB.getId());
            intent.putExtra(AppConstant.INTENT_NAME, repositoryDB.getName());
            intent.putExtra(AppConstant.INTENT_SUB_TITLE, repositoryDB.getOwner().getLogin());
            context.startActivity(intent);
        }

        void bind(Repository repository) {
            binding.setVariable(BR.viewModel, repository);
            binding.executePendingBindings();
        }
    }
}
