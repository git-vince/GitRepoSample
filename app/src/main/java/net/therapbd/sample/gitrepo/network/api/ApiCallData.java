package net.therapbd.sample.gitrepo.network.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiCallData {

    @GET("orgs/{org_name}/repos")
    Call<ResponseBody> requestRepositoryList(@Path("org_name") String orgName);

}
