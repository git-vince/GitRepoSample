package net.therapbd.sample.gitrepo.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RepositoryNote extends RealmObject {
    @PrimaryKey
    private int noteId;

    private String note;

    private long lastSavedTime = 0;

    public RepositoryNote() {
    }

    public RepositoryNote(int noteId) {
        this.noteId = noteId;
    }

    public RepositoryNote(int noteId, String note, long lastSavedTime) {
        this(noteId);
        setNote(note);
        setLastSavedTime(lastSavedTime);
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getLastSavedTime() {
        return lastSavedTime;
    }

    public void setLastSavedTime(long lastSavedTime) {
        this.lastSavedTime = lastSavedTime;
    }
}
