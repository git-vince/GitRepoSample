package net.therapbd.sample.gitrepo.network.client;

public class ResponseCode {

    public static final int SUCCESSFUL = 200;

    public static final int PARSE_ERROR = 0;
    public static final int NETWORK_ERROR = 1;
    public static final int IO_ERROR = 2;
}
