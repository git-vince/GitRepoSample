package net.therapbd.sample.gitrepo.database;

import net.therapbd.sample.gitrepo.application.GitRepoApp;
import net.therapbd.sample.gitrepo.database.dbmodel.RepositoryDB;
import net.therapbd.sample.gitrepo.model.RepositoryNote;

import io.realm.RealmResults;

public class DatabaseTransactions {
    private static DatabaseTransactions databaseTransactions;

    private DatabaseTransactions() {
    }

    synchronized public static DatabaseTransactions getDatabaseTransactions() {
        if (databaseTransactions == null)
            databaseTransactions = new DatabaseTransactions();
        return databaseTransactions;
    }

    public void insertRepositories(String jsonResponse) {
        GitRepoApp.getApplication().getRealm().executeTransaction(realm -> {
            realm.createOrUpdateAllFromJson(RepositoryDB.class, jsonResponse);
        });
    }

    public RealmResults<RepositoryDB> retrieveRepositories() {
        return GitRepoApp.getApplication().getRealm().where(RepositoryDB.class).findAll();
    }

    public void createOrUpdateRepositoryNote(RepositoryNote repositoryNote) {
        GitRepoApp.getApplication().getRealm().executeTransaction(realm -> {
            realm.insertOrUpdate(repositoryNote);
        });
    }

    public RepositoryNote retrieveRepositoryNote(int repositoryId) {
        return GitRepoApp.getApplication().getRealm().where(RepositoryNote.class).equalTo("noteId", repositoryId).findFirst();
    }

}