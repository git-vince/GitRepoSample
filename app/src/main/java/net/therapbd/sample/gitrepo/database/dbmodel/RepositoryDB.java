package net.therapbd.sample.gitrepo.database.dbmodel;



import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RepositoryDB extends RealmObject {

    @PrimaryKey
    private Integer id;
    private String name;
    private OwnerDB owner;
    private String description;
    private Boolean fork;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OwnerDB getOwner() {
        return owner;
    }

    public void setOwner(OwnerDB owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getFork() {
        return fork;
    }

    public void setFork(Boolean fork) {
        this.fork = fork;
    }
}
