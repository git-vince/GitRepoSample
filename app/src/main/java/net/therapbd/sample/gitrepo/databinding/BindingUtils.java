package net.therapbd.sample.gitrepo.databinding;

import android.view.View;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import net.therapbd.sample.gitrepo.R;
import net.therapbd.sample.gitrepo.application.GitRepoApp;

import java.util.List;

public class BindingUtils {

    @BindingAdapter("data")
    public static <T> void setRecyclerViewProperties(RecyclerView recyclerView, List<T> items) {

        if (recyclerView.getAdapter() instanceof BindableListAdapter<?>) {
            ((BindableListAdapter<T>) recyclerView.getAdapter()).setData(items);

        }
    }

    @BindingAdapter("adapter")
    public static <T extends RecyclerView.ViewHolder> void setRecyclerViewAdapter(RecyclerView recyclerView, RecyclerView.Adapter<T> adapter) {
        if (adapter != null)
            recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("setSwipeRefreshing")
    public static void setSwipeRefreshing(SwipeRefreshLayout swipeRefreshLayout, boolean isLoading) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(isLoading);
    }

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, Boolean value) {
        view.setVisibility(value ? View.VISIBLE : View.GONE);
    }


    @BindingAdapter("android:src")
    public static void setImageSrc(ImageView imageView, String imageUrl) {
        GitRepoApp.getApplication().getPicasso()
                .load(imageUrl)
                .placeholder(R.drawable.ic_img_not_found)
                .error(R.drawable.ic_img_not_found).fit().noFade()
                .into(imageView);
    }

}
