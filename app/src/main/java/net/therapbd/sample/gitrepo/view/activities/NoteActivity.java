package net.therapbd.sample.gitrepo.view.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import net.therapbd.sample.gitrepo.R;
import net.therapbd.sample.gitrepo.databinding.ActivityNoteBinding;
import net.therapbd.sample.gitrepo.util.AppConstant;
import net.therapbd.sample.gitrepo.viewmodel.NoteViewModel;

import static net.therapbd.sample.gitrepo.util.AppConstant.REPOSITORY_ID_NOT_FOUND;


public class NoteActivity extends BaseActivity {

    private NoteViewModel noteViewModel;
    private int repositoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();

        ActivityNoteBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_note);

        noteViewModel = new NoteViewModel(repositoryId);
        noteViewModel.loadNoteFromDB();

        binding.setViewModel(noteViewModel);

        binding.save.setOnClickListener(v -> {
            if (noteViewModel.isNoteDataChanged.get())
                noteViewModel.saveNote();
        });
    }

    @Override
    public void onBackPressed() {
        if (noteViewModel.isNoteDataChanged.get()) {
            showAlert();
        } else {
            super.onBackPressed();
        }
    }

    private void init() {

        if (getIntent() == null) {
            Toast.makeText(this, getString(R.string.invalid_argument), Toast.LENGTH_LONG).show();
            finish();
        }
        repositoryId = getIntent().getIntExtra(AppConstant.INTENT_ID, REPOSITORY_ID_NOT_FOUND);
        String actionBarTitle = getIntent().getStringExtra(AppConstant.INTENT_NAME);
        String actionBarSubTitle = getIntent().getStringExtra(AppConstant.INTENT_SUB_TITLE);

        if (repositoryId == REPOSITORY_ID_NOT_FOUND) {
            Toast.makeText(this, getResources().getString(R.string.invalid_repository_label), Toast.LENGTH_LONG).show();
            finish();
        } else if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(actionBarTitle);
            getSupportActionBar().setSubtitle(actionBarSubTitle);
        }
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.unsaved_changes_alert));
        builder.setMessage(getResources().getString(R.string.unsaved_changes_alert_body));
        builder.setNeutralButton(getResources().getString(R.string.cancel_option), (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton(getResources().getString(R.string.discard_option), (dialog, which) -> {
            dialog.dismiss();
            finish();
        });
        builder.setNegativeButton(getResources().getString(R.string.save_and_exit_option), (dialog, which) -> {
            dialog.dismiss();
            noteViewModel.saveNote();
            finish();
        });
        builder.show();
    }

}
