package net.therapbd.sample.gitrepo.view.activities;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import net.therapbd.sample.gitrepo.R;
import net.therapbd.sample.gitrepo.databinding.ActivityMainBinding;
import net.therapbd.sample.gitrepo.viewmodel.MainViewModel;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    @Inject
    MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = new MainViewModel();
        viewModel.populateRepositoryList();

        binding.setViewModel(viewModel);

    }

}
