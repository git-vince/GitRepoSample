package net.therapbd.sample.gitrepo.network.client;

import net.therapbd.sample.gitrepo.network.api.APIs;
import net.therapbd.sample.gitrepo.network.api.ApiCallData;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {

    private static Retrofit retrofitInstance = null;

    private NetworkClient() {}

    private static synchronized Retrofit getNetworkClient() {
        if (retrofitInstance == null) {
            OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
            okHttpClientBuilder.connectTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES);

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
            okHttpClientBuilder.addInterceptor(logging);

//        okHttpClientBuilder.addInterceptor(chain -> {
//            Request original = chain.request();
//            Request request = original.newBuilder()
//                    .header("Authorization", User.getUser().getToken())
//                    .header("Content-Type", "application/json")
//                    .method(original.method(),original.body())
//                    .build();
//            return chain.proceed(request);
//        }).build();

            retrofitInstance = new Retrofit.Builder()
                    .baseUrl(APIs.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClientBuilder.build())
                    .build();
        }
        return retrofitInstance;
    }

    public static ApiCallData getApiService() {
        return getNetworkClient().create(ApiCallData.class);
    }

}
