package net.therapbd.sample.gitrepo.viewmodel;

import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableBoolean;

public class BaseViewModel extends BaseObservable {
    public ObservableBoolean isLoading = new ObservableBoolean(false);
    public ObservableBoolean isNoDataFound = new ObservableBoolean(false);
}
