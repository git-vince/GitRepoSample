package net.therapbd.sample.gitrepo.application;

import com.squareup.picasso.Picasso;

import net.therapbd.sample.gitrepo.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class GitRepoApp extends DaggerApplication {

    private static GitRepoApp application;
    private Picasso picasso;
    private Realm realm;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    public static GitRepoApp getApplication() {
        return application;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        closeRealm();
    }

    public Picasso getPicasso() {
        if (picasso == null) {
            picasso = Picasso.get();
            picasso.setLoggingEnabled(false);
        }
        return picasso;
    }

    public Realm getRealm() {
        if (realm == null || realm.isClosed())
            realm = Realm.getDefaultInstance();
        return realm;
    }

    public void closeRealm() {
        if (realm != null && !realm.isClosed())
            realm.isClosed();
    }
}
